# OpenML dataset: kidney

https://www.openml.org/d/553

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: McGilchrist and Aisbett  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 1999  
**Please cite**:   

Data on the recurrence times to infection, at the point of insertion of the catheter, for kidney patients using portable dialysis equipment. Catheters may be removed for reasons other than infection, in which case the observation is censored.  Each patient has exactly 2 observations.

The data set has been used by several authors to illustrate random effects ("frailty") models for survival data. However, any non-zero estimate of the random effect is almost entirely due to one outlier, subject 21.

Variables: patient, time, status, age, sex (1=male, 2=female), disease type (0=Glomerulo Nephritis, 1=Acute Nephritis,
2=Polycystic Kidney Disease, 3=Other), author's estimate of the frailty

References:  
McGilchrist and Aisbett, Biometrics 47, 461-66, 1991

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/553) of an [OpenML dataset](https://www.openml.org/d/553). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/553/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/553/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/553/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

